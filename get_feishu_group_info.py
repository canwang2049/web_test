#!/bin/env python3
import re
import argparse
import os
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
import json
import logging
import sys
import pyautogui


# Configure the root logger

logging.basicConfig(
    level=logging.INFO,  # Set the root logger's logging level
    # stream=sys.stdout,  # Output to stdout
    format="%(levelname)s %(filename)s:%(lineno)d %(message)s",  # Use our custom formatter
)


def demo():
    driver = webdriver.Edge()
    driver.get("http://www.python.org")
    assert "Python" in driver.title
    elem = driver.find_element(By.NAME, "q")
    elem.clear()
    elem.send_keys("pycon")
    elem.send_keys(Keys.RETURN)
    assert "No results found." not in driver.page_source
    driver.close()


home_dir = os.path.expanduser("~")
edge_exe = "/opt/microsoft/msedge/microsoft-edge"
edge_profile_path = f"{home_dir}/.config/microsoft-edge/Default"


def load_cookies() -> list:
    # 使用 Chrome 插件 `EditThisCookie` 来导出 cookies 列表，粘入此文件
    cookie_file = "data/feishu_cookie.json"
    with open(cookie_file, "r", encoding="utf8") as f:
        text = f.read()
        text = re.sub('"sameSite": "(.+?)",', '"sameSite": "None",', text)
    cookies = json.loads(text)
    current_domain = ".feishu.cn"
    for cookie in cookies:
        cookie["domain"] = current_domain
    return cookies


def open_group(driver, wait, group_name):
    search_box_class_name = "quick-jump-enter-com__box"
    wait.until(
        EC.element_to_be_clickable((By.CLASS_NAME, search_box_class_name))
    ).click()
    search_input_box_class_name = "quickJump_input"
    wait.until(
        EC.element_to_be_clickable((By.CLASS_NAME, search_input_box_class_name))
    ).send_keys(group_name)
    logging.info("search group")
    first_search_result_xpath = "//div[@class='quickJump_resultContainer']/div[2]"
    wait.until(
        EC.visibility_of_element_located((By.XPATH, first_search_result_xpath))
    ).click()
    logging.info("click first search result")


def get_member_count(driver, wait):
    member_count_class_name = "groupAuth_title"
    wait.until(
        EC.visibility_of_element_located((By.CLASS_NAME, member_count_class_name))
    )
    member_count_txt = driver.find_element(By.CLASS_NAME, member_count_class_name).text
    member_count = re.search(r"\((\d+)\)", member_count_txt)
    if member_count:
        number_part = member_count.group(1)
        logging.info(f"member count: {number_part}")
        return int(number_part)
    return 0


def open_member_list(driver, wait):
    logging.info("begin to open member list")
    chat_tile_class_name = "chatWindow_avatar"
    wait.until(
        EC.element_to_be_clickable((By.CLASS_NAME, chat_tile_class_name))
    ).click()


def get_user_info(driver, wait):
    user = {}
    name = wait.until(
        EC.visibility_of_element_located(
            (By.XPATH, '//div[@class="larkc-usercard-name-wrapper"]/h3')
        )
    ).text
    user["name"] = name
    logging.info(f"name: {name}")
    user_card_main_class_name = "larkc-usercard-main"
    main_info = driver.find_element(By.CLASS_NAME, user_card_main_class_name)
    # logging.info(f"elements: {main_info}")
    style_info = main_info.get_attribute("style")
    logging.info(f"style: {style_info}")
    # Using regular expression to extract the value
    match = re.search(r"--usercard-fields-length:\s*(\d+)", style_info)
    k_v_count = (int)(match.group(1))
    logging.info(f"k_v_count: {k_v_count}")
    for i in range(0, k_v_count):
        key_xpath = f"//div[@class='larkc-usercard-main']/div[{i*2+1}]"
        value_xpath = f"//div[@class='larkc-usercard-main']/div[{i*2+2}]"
        key = driver.find_element(By.XPATH, key_xpath).text
        value = driver.find_element(By.XPATH, value_xpath).text
        key.encode("utf-8")
        value.encode("utf-8")
        logging.info(f"key: {key}, value: {value}")
        user[key] = value

    return user


def esc():
    pyautogui.press("esc")
    pyautogui.press("esc")
    pyautogui.press("esc")
    pyautogui.press("esc")
    pyautogui.press("esc")
    pyautogui.press("esc")
    pyautogui.press("esc")
    pyautogui.press("esc")
    pyautogui.press("esc")


def open_user_card(driver, wait, user_index):
    logging.info("begin to visit every member")
    # click to show original member list\
    user_card_xpath = (
        f"//div[@class='groupAuth_list groupAuth_memberList']/div[{user_index}]/div[1]"
    )
    logging.info(f'xpath:    $x("{user_card_xpath}")')
    wait.until(EC.element_to_be_clickable((By.XPATH, user_card_xpath))).click()
    time.sleep(1)


def main(args):
    options = webdriver.EdgeOptions()
    driver = webdriver.Edge(options=options)
    driver.maximize_window()
    wait = WebDriverWait(driver, 500)
    esc()

    feishu_cn_url = "https://feishu.cn"
    feishu_messager_url = "https://www.feishu.cn/messenger/"

    driver.get(feishu_messager_url)
    esc()

    cookies = load_cookies()

    for cookie in cookies:
        driver.add_cookie(cookie)

    driver.get(feishu_messager_url)

    open_group(driver, wait, args.group)
    esc()
    open_member_list(driver, wait)
    # driver.maximize_window()
    member_count = get_member_count(driver, wait)
    logging.info(f"get member count: {member_count}")
    if member_count == 0:
        logging.error("no member in this group, exit")
        return

    esc()
    for idx in range(2, member_count + 2):
        logging.info(f"begin of visiting user index: {idx}")

        # driver.find_element(By.TAG_NAME, "body").send_keys(Keys.ESCAPE)
        esc()
        open_member_list(driver, wait)

        open_user_card(driver, wait, idx)
        get_user_info(driver, wait)
        logging.info(f"end of visiting user index: {idx}")

        break

    # time.sleep(5)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--group", type=str, help="group name to search for")
    args = parser.parse_args()
    logging.info(f"args: {args}")
    main(args)
